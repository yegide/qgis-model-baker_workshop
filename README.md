# QGIS Model Baker Workshop
## Generating QGIS projects from data models with Postgis or GPKG data sources

### Software requirements

For the workshop you will need the following software:

* A current [QGIS version](https://download.qgis.org/) (3.4 or 3.6)
* A local [Postgis](https://www.postgis.net/) installation. For Windows it is part of the [PostgreSQL EnterpriseDB downloader](https://www.postgresql.org/download/windows/). On Linux please install from the packages of your distribution.
* A decent text editor (on Windows e.g. [Notepad++](https://notepad-plus-plus.org/), Ultraedit; on MacOS something like BBEdit on Linux something lake Kate, EMacs, vim or similar)
* A current [Java runtime environment](https://www.java.com/de/download/) (required for Model Baker, ili2pg and the UML editor). On Linux please use the JRE from your distribution.
* The java-based [Interlis UML editor](https://www.interlis.ch/en/downloads/umleditor) - the UML editor can be started in the languages en, de, fr and it by using the following start command from the console or DOS-Window:
* [UML Editor (Updated and Spanish support)](https://tinyurl.com/y4j7fseq)


`java -Duser.language=en -jar umleditor-3.7.0.jar`

### Presentation on data modeling and QGIS Model Baker

[Presentation an Google Docs](https://docs.google.com/presentation/d/1pvSnU_FNkj8uwxFSPfIRQX9o8NkCjy0XyAa3crRK3_I/edit?usp=sharing)

### Exercise

#### Exercise 1
Load existing data model from [GL_Busstops Interlis-File describing bus stops](https://gitlab.com/aneumann/qgis-model-baker_workshop/raw/master/GL_Busstops_V_2_1_en.ili). See also [UML diagram](https://gitlab.com/aneumann/qgis-model-baker_workshop/blob/master/GL_Busstops_V_2_1_en.png) of the data model.

*  Save GL_Bus stops ili file to your local hard drive
*  Start QGIS Model Baker: Menu "Database" > "Model Baker" > "Generate"
*  After import, examine data structure in pgAdmin
*  After import, examine project structure in QGIS
*  Try to add a new station and related objects
*  Improve widget configuration

#### Exercise 2
Create your own data model using either text editor or UML editor.

* In the model tree in the left panel, navigate to "Predefined" > Right click and add Model > give it a name
* In the model > right click and add topic > give it a name
* In the topic > right click and add a class (table) > give it a name
* double click the class to edit its properties
* got to attributes tab and add attributes (table columns)
* Right click an use new > choose data types and edit 
* Add domains with enumerations for value lists
* Add relationship for associations

Save the model to a .uml file and as a second step export it to an Interlis ili model file (Menu Tools > Interlis > Export)

Do the same steps as above with your own data model.